# Fedora Toolbox Custom

A personal custom image for Toolbox.

To create a toolbox with this image: `toolbox create -i registry.gitlab.com/unshippedreminder/fedora-toolbox-custom`