#!/bin/bash

echo "Setting up..."
export container_name='fedora-toolbox-working-container'
buildah from quay.io/fedora/fedora-toolbox:latest

echo "Configuring container..."
buildah config --label com.github.containers.toolbox="true" \
               --label usage="This image is meant to be used with the toolbox command" \
               --label summary="Personal Toolbox Image" $container_name

echo "Copying files..."
buildah copy $container_name ./dnf-packages /dnf-packages
buildah copy $container_name ./pypi-packages /pypi-packages
buildah copy --chown root:root --chmod 755 $container_name ./host-runner /usr/local/bin/host-runner

echo "Linking host-runner to host binaries..."
buildah run $container_name ln -s /usr/local/bin/host-runner /usr/local/bin/rpm-ostree
buildah run $container_name ln -s /usr/local/bin/host-runner /usr/local/bin/podman
buildah run $container_name ln -s /usr/local/bin/host-runner /usr/local/bin/flatpak
buildah run $container_name ln -s /usr/local/bin/host-runner /usr/local/bin/virsh
buildah run $container_name ln -s /usr/local/bin/host-runner /usr/local/bin/firefox
buildah run $container_name ln -s /usr/local/bin/host-runner /usr/local/bin/nautilus
buildah run $container_name ln -s /usr/local/bin/host-runner /usr/local/bin/systemctl


echo "Building container..."
buildah run $container_name bash -c 'echo "fastestmirror=true" >> /etc/dnf/dnf.conf'
buildah run $container_name dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
                                           https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
buildah run $container_name dnf upgrade -y
echo "Installing custom packages..."
buildah run $container_name dnf install -y $(<dnf-packages)
buildah run $container_name pip install $(<pypi-packages)
echo "Installing xonsh-venv..."
buildah run $container_name git clone https://gitlab.com/unshippedreminder/xonsh-venv.git /var/opt/xonsh-venv
buildah run --workingdir /var/opt/xonsh-venv/bin $container_name bash -c "source ./activate && ./python -m ensurepip"
buildah run --workingdir /var/opt/xonsh-venv/bin $container_name ./upgrade-xonsh-venv.sh

echo "Cleaning..."
buildah run $container_name rm /dnf-packages 
buildah run $container_name pip cache purge
buildah run $container_name rm /pypi-packages 
buildah run --workingdir /var/opt/xonsh-venv/bin $container_name ./pip cache purge

echo "Patching /etc/sudo.conf to fix https://github.com/containers/toolbox/issues/1058"
buildah run $container_name bash -c "echo '%wheel ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/nopasswd"

buildah run $container_name dnf clean all

echo "Pusing image to registry..."
buildah commit $container_name ${CI_PROJECT_NAME}:latest
